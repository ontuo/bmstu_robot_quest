## На raspberry pi:

```console
    $ sudo apt install python3-pip
    $ pip3 install pyserial
    $ cd /home/pi/
    $ mkdir robo_vacuum
```
В папку robo_vacuum поместить скрипт uart.py.

## Hа компьютере:

```console
    $ sudo apt install nmap
    $ sudo apt install sshpass
```
1. Найти свой ip и маску:
```console
$ ip address show
```
2. Воспользоваться nmap для того, чтобы найти ip raspberry pi:
```console
$ nmap -sn <IP_from_1st_step>
```
3. Создать файл <name>.txt. В этот файл записываются команды для робота (список далее). Одна команда на строку.
4. Запустить следующий скрипт:
```console
        $ bash send_commands.sh <raspberry_ip> <name>.txt
```
## Список команд
0. forward  -- робот двигается вперед на 450 мм
1. back     -- робот двигается назад на 450 мм
2. left     -- робот поворачивается налево на 90 градусов
3. right    -- робот поворачивается направо на 90 градусов
4. turn180  -- робот поворачивается на 180 градусов
5. beep     -- звук
6. tutu     -- звук
7. song     -- звук

пункты 5,6,7 могут не работать на некоторых моделях роботов 