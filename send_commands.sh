#!/bin/bash

sshpass -p "raspberry" scp $2 pi@$1:/home/pi/robo_vacuum 
sshpass -p "raspberry" ssh -o StrictHostKeyChecking=no pi@$1 "python3 /home/pi/robo_vacuum/uart.py /dev/ttyACM0 /home/pi/robo_vacuum/$2"