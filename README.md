# BMSTU_robot_quest

## Commands

0. forward  -- robot goes forward for 300 mm 
1. back     -- robot goes backward for 300 mm
2. left     -- robot turns left for 90 deg
3. right    -- robot turns right for 90 deg
4. turn180  -- robot turns for 180 deg
5. beep     -- sound
6. tutu     -- sound
7. song     -- sound

(5, 6, 7 might not work on some robots) 

Commands should be written in txt file one command per line

## What to do?

1. Find out your ip:
```console
$ ip address show
```
2. Use nmap to find raspberry ip:
```console
$ nmap -sn <IP_from_1st_step>
```
3. Create <name>.txt file and write down the commands.
4. Issue this:
```console
        $ bash send_commands.sh <raspberry_ip> <name>.txt
```
5. Watch your robot moving :)

## What to install?

You need to install nmap and sshpass on your computer:
```console
    $ sudo apt install nmap
    $ sudo apt install sshpass
```
And install pyserial  (and pip if u dont have it) on raspberry:
```console
    $ sudo apt install python3-pip
    $ pip3 install pyserial
```