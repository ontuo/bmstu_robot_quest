#!/usr/bin/python3
import serial
import sys
import time

turn_sleeptime = 4
turn_180_sleeptime = 4
straight_sleeptime = 1


vacuum_serial = serial.Serial(sys.argv[1], 115200)
vacuum_serial.write(str.encode("testmode on \n"))
time.sleep(2)

with open(sys.argv[2], "r") as i:
    lines = i.readlines()


for i in lines:
    if i.find("forward") != -1:
        vacuum_serial.write(str.encode("SetMotor LWheelDist 450 RWheelDist 450 Speed 250 \n"))
        time.sleep(straight_sleeptime)
    if i.find("left") != -1:
        vacuum_serial.write(str.encode("SetMotor LWheelDist -190 RWheelDist 190 Speed 250 \n"))
        time.sleep(turn_sleeptime)
    if i.find("right") != -1:
        vacuum_serial.write(str.encode("SetMotor LWheelDist 190 RWheelDist -190 Speed 250 \n"))
        time.sleep(turn_sleeptime)
    if i.find("back") != -1:
        vacuum_serial.write(str.encode("SetMotor LWheelDist -450 RWheelDist -450 Speed 150 \n"))
        time.sleep(straight_sleeptime)
    if i.find("turn180") != -1:
        vacuum_serial.write(str.encode("SetMotor LWheelDist -390 RWheelDist 390 Speed 200 \n"))
        time.sleep(turn_180_sleeptime)
    if i.find("beep") != -1:
        vacuum_serial.write(str.encode("PlaySound 3\n"))
        time.sleep(2)
    if i.find("tutu") != -1:
        vacuum_serial.write(str.encode("PlaySound 0\n"))
        time.sleep(2)
    if i.find("song") != -1:
        vacuum_serial.write(str.encode("PlaySound 1\n"))
        time.sleep(2)

vacuum_serial.write(str.encode("testmode off \n"))
vacuum_serial.close()